function Person(props) {
    return (
        <div className="col-sm-6">
            <div className="card">
                <div className="card-body">
                    <h5 className="card-title">React component</h5>
                    <p className="card-text">Name: {props.name}</p>
                    <p className="card-text">Age: {props.age}</p>
                </div>
            </div>
        </div>
    );
};

var app = (
    <div className="row">
        <Person name="Artem" age="77" />
        <Person name="Nemrosim" age="55" />
    </div>
);

ReactDOM.render(app,
    document.querySelector('#app'));

// ReactDOM.render(<Person name="Nemrosim" age="55" />,
//     document.querySelector('#personReactTwo'));