var socket = io();

// build-in events
socket.on('connection', function () {
    console.log('Connected to server!');

    // Second argument is what we sending, emiting...

});

socket.emit('createEmail', {
    to: 'nemrosim@example.com',
    text: 'Hello World!'
});

socket.on('disconnect', function () {
    console.log('Disconnected from server');
});

// custom events
socket.on('newEmail', function (email) {
    console.log('Sending email', email);
});

/**
 * 
 */
socket.on('newMessage', function (message) {
    console.log('newMessage', message);

    // var formattedTime = moment(message.createdAt);
    var formattedTime = moment(message.createdAt).format('h:mm a');
    

    console.log('formattedTime:', formattedTime);

    var li = jQuery(`<li class="list-group-item"></li>`);
    li.text(`${message.from} ${formattedTime}: ${message.text}`)
    jQuery('#messageList').append(li);
});

/**
 * jQuery.on -> eventListener
 */
jQuery('#messageForm').on('submit', function (e) {
    // We need to rewrite default behavior of onlick submit action (refreshing all page)
    e.preventDefault();

    // [] -> name     # -> id
    socket.emit('createMessage', {
        from: 'Artem',
        text: jQuery('[name=testMessage]').val()
    }, function (data) {
        console.log('Working!', data);

        //setting to empty string
        jQuery('[name=testMessage]').val('');
    });
});

/**
 * 
 */
var locationButton = jQuery('#send-location');
locationButton.on('click', function () {
    if (!navigator.geolocation) {
        return alert('Geolocation not supported');
    }

    locationButton.attr('disabled', 'disabled');

    navigator.geolocation.getCurrentPosition(function (position) {
        locationButton.removeAttr('disabled');
        socket.emit('createLocationMessage', {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
        });
    }, function () {
        locationButton.removeAttr('disabled');
        alert('Unable to fetch location')
    });
});

/**
 * 
 */
socket.on('newLocationMessage', function (message) {

    var formattedTime = moment(message.createdAt).format('h:mm a');

    var li = jQuery(`<li class="list-group-item"></li>`);
    var a = jQuery(`<a target="_blank">My location</a>`);
    // BLANK -> OPEN NEW TAB
    li.text(`${message.from} ${formattedTime}:`);
    a.attr('href', message.url);
    li.append(a);

    jQuery('#messageList').append(li);
});