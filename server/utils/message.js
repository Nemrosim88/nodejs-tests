var moment = require('moment');


var generateMessage = (from, text) => {
    return {
        from,
        text,
        createdAt: moment().valueOf()
    };
};

var generateLocationMessage = (from, latitude, longitude) => {
    return {
        from,
        url: `https://www.google.ru/maps?q=${latitude},${longitude}`,
        createdAt: new Date().getTime()
    };
};

module.exports = { generateMessage, generateLocationMessage };
