const path = require('path');
const http = require('http');
const express = require('express');
const socketIO = require('socket.io');

const { generateMessage, generateLocationMessage } = require('./utils/message');

var app = express();

const publicPath = path.join(__dirname, '../public');
app.use(express.static(publicPath));


/**
 * This method is used behind the scene in app.listen.
 * var server = http.createServer((req, res) => {}); ---> Variant 1
 */
var server = http.createServer(app); // ----> Variant 2 EQUAL
var io = socketIO(server);

io.on('connection', function (socket) {
    console.log('New user connected');

    /**
     * Emiting events. This one in index.js
     */
    socket.emit('newEmail', generateMessage('artem@example.com', 'Hello'));

    /**
     * 
     */
    socket.broadcast.emit('newMessage', generateMessage('Admin', 'New user connected'));

    /**
     * 
     */
    socket.on('createEmail', (newEmail) => { console.log('createEmail', newEmail); });

    /**
     * 
     */
    socket.on('disconnect', () => { console.log('User was disconnected!'); });

    /**
     * 
     */
    socket.on('createMessage', function (message, callback) {
        console.log('createMessage', message);

        //For every single connection -> io.emit. socket.emit -> for one.        
        io.emit('newMessage', generateMessage(message.from, message.text));


        //Для возврата сообщения клиенту, что всё прошло удачно.         
        callback('Hello');

        // Перед заменой на метод generateMessage -> было так
        // {
        //     from: message.from,
        //     text: message.text,
        //     createdAt: new Date().getTime()
        // }

        /**
         * Sending message for all but not for me.
         */
        // socket.broadcast.emit('newMessage', {
        //   from: message.from,
        //   text: message.text,
        //   createdAt: new Date().getTime()
        // });
    });


    socket.on('createLocationMessage', (coords) => {
        io.emit('newLocationMessage', generateLocationMessage('Admin', coords.latitude, coords.longitude));
    });
});


const port = process.env.PORT || 3000;
server.listen(port, () => {
    console.log(`Server is running on port ${port}`)
});
